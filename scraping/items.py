import scrapy


class ScrapingItem(scrapy.Item):
    
    name = scrapy.Field()
    img_src = scrapy.Field()
