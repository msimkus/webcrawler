from sqlalchemy.orm import sessionmaker
from scraping.models import Car, db_connect, create_table


class ScrapingPipeline(object):
    
    def __init__(self):
        """
        Initializes database connection and sessionmaker
        Creates tables
        """
        
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind = engine)


    def process_item(self, item, spider):
        """Save cars in the database
        This method is called for every item pipeline component
        """
        
        session = self.Session()
        
        # check if the entry does not already exist in the database
        if session.query(Car).filter_by(
                name = item["name"],
                img_src = item["img_src"]
                ).first() is None:
            
            car = Car()
            car.name = item["name"]
            car.img_src = item["img_src"]
            
            try:
                session.add(car)
                session.commit()
            except:
                session.rollback()
                raise
            finally:
                session.close()

        return item
