import scrapy

import sys
import os
# necessary if the file is called from the "webcrawler" directory
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from scraping.items import ScrapingItem

class AutopliusSpider(scrapy.Spider):
    
    name = "autoplius"
    #allowed_domains = ["https://m.autoplius.lt/"]
    start_urls = ["https://m.autoplius.lt/skelbimai/naudoti-automobiliai"]
    
    def parse(self, response):
        
        # collect car names
        names = response.xpath(
            "//strong[@class = 'title-list']/text()").extract()
        
        # collect sources of car images
        img_srcs = response.xpath(
            "//span[@class = 'thumb-inner']/img/@src").extract()
        img_srcs = list(filter(None, img_srcs))
        
        # extend the list containing sources of car images
        # with image sources in "noscript" tags
        img_srcs.extend(response.xpath(
            "//span[@class = 'thumb-inner']/descendant::*[not(self::script)]"\
            "/img/@src").extract())
        
        # the length of lists containing car names
        # and image sources should be equal
        img_srcs = img_srcs[0:len(names)]
        
        scraping_item = ScrapingItem()
        
        for name, img_src in zip(names, img_srcs):
            
            # remove redundant characters
            name = name.replace("\n", "")
            name = name.strip()
            
            # put the extracted data into the scraping item
            scraping_item["name"] = name
            scraping_item["img_src"] = img_src
            
            # save the record in the database
            yield scraping_item

        # if possible, collect data from the next page
        next_url_path = response.xpath(
            "//div[@class = 'paging']/a[@class = 'fr btn button right-nav']"\
            "/@href").extract_first()
        
# =============================================================================
#         if next_url_path:
#             yield scrapy.Request(
#                 response.urljoin(next_url_path),
#                 callback = self.parse
#             )
# =============================================================================

# run the script by using the following command
# scrapy runspider autoplius.py
