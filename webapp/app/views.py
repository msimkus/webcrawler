import os
import sys
import subprocess

from flask import render_template
from app import app

# necessary if the file is called from the "webcrawler" directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
from scraping.models import db_connect

from sqlalchemy.orm import sessionmaker
from sqlalchemy import text


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/scraper_output')
def run_scraper():
    
    app_path = os.path.join(app.instance_path.replace("webapp\instance", ""),
                            "scraping\spiders")
    spider_name = "autoplius.py"
    
    # try to execute scraping procedures
    try:
        subprocess.run([
            "scrapy",
            "runspider",
            os.path.join(app_path, spider_name)],
            shell = True
        )
    except:
        print(f"Could not execute the command that runs {spider_name}")
    
    # fetch the data from the database
    engine = db_connect()
    Session = sessionmaker(bind = engine)
    session = Session()
    sql_read = text("select * from car;")
    data = session.execute(sql_read)
    
    return render_template('scraper_output.html',
                           car_data = data)
 