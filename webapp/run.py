# run.py

from app import app

if __name__ == '__main__':
    app.run()

# execute the following commands in order to run Flask
# set FLASK_APP=run.py
# flask run